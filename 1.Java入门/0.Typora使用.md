# Typora使用



## Typora基础输入

+ 介绍一些typora的常用符号以及快捷键等，有些较为少见，很难搜到。

  | 功能需求     | 快捷键                | 源码格式                          |
  | ------------ | --------------------- | --------------------------------- |
  | 有序列表     | ctrl + shift + [      | 数字.                             |
  | 无序列表     | ctrl + shift + ]      | +  或  -  或  *                   |
  | 任务列表     |                       | - [ ]  - [x]                      |
  | 表格         | ctrl + t              | \| \| \| \|                       |
  | 表格下移该行 | alt + ↓               |                                   |
  | 表格上移该行 | alt + ↑               |                                   |
  | 表格左移该列 | alt + ←               |                                   |
  | 表格右移该列 | alt + →               |                                   |
  | 表格添加行   | table 或 ctrl + enter |                                   |
  | 表格删除行   | ctrl + shift + delete |                                   |
  | 标题         | ctrl + 1~6            | #                                 |
  | 引用         | ctrl + shift + q      | >                                 |
  | 分割线       |                       | --- 或 *** 或 +++                 |
  | 倾斜         | ctrl + i              | *倾斜* _(或 _ 符号)_              |
  | 加粗         | ctrl + b              | **加粗** __(或 _ 符号)__          |
  | 倾斜加粗     |                       | ***倾斜加粗***  ___(或 _ 符号)___ |
  | 删除线       | alt + shift + 5       | ~~删除线~~                        |
  | 下划线       | ctrl + u              | <u>下划线</u>                     |
  | 行内代码     | ctrl + shift + `      | `行内代码`                        |
  | 代码块       | ctrl + shift + k      | ``` 代码块```                     |
  | 公式块       | ctrl + shift + m      | $$                                |
  | 脚注         |                       | 内容[^脚注]                       |
  | 超连接       | ctrl + k              | [百度](www.baidu.com)             |
  | 图片         | ctrl + shift + i      | ![文本](图片链接)                 |
  | 选中一整行   | ctrl + l              |                                   |
  | 选中单词     | ctrl + d              |                                   |
  | 替换         | ctrl + h              |                                   |
  | 搜索         | ctrl + f              |                                   |







## Typora图表



### 1.UML序列图

+ UML序列图采用 sequence 语法，仅支持以下几种：

  ```sequence
  # 所有语法，示例程序都包含在内
  Title: ExampleTitle
  A->B: Normal line
  B-->C: Dashed line
  C->>D: Open arrow
  D-->>A: Dashed open arrow
  
  Note left of A: Note to the \n left of A
  Note right of A: Note to the\n right of A
  Note over A: Note over A
  Note over A,B: Note over both A and B
  
  participant E
  participant F
  Note right of F: By listing the participants\n you can change their order
  ```

  









### 2.标准流程图

+ 流程图 flowchart ，标签选择flow：

  ```flow
  st=>start: 开始框
  op=>operation: 处理框
  cond=>condition: 判断框(是或否?)
  sub1=>subroutine: 子流程
  io=>inputoutput: 输入输出框
  e=>end: 结束框
  
  st->op->cond
  cond(yes)->io->e
  cond(no)->sub1(right)->op
  ```










+ 横向流程图

  ```flow
  st=>start: 开始框
  op=>operation: 处理框
  cond=>condition: 判断框(是或否?)
  sub1=>subroutine: 子流程
  io=>inputoutput: 输入输出框
  e=>end: 结束框
  
  st(right)->op(right)->cond
  cond(yes)->io(bottom)->e
  cond(no)->sub1(right)->op
  ```

  













### 3.Mermaid

+ Typora 还与 mermaid 集成，它支持序列图、流程图、甘特图表、类和状态图以及饼图。

​	

#### 3.1.序列图

```mermaid
%% Example of sequence diagram
%% -> 直线，-->虚线，->>实线箭头
sequenceDiagram
participant 张三
participant 李四

张三->王五: 王五你好吗？

loop 健康检查
王五->王五: 与疾病战斗
end

Note right of 王五: 合理 食物 <br/>看医生...

李四-->>张三: 很好!
王五->李四: 你怎么样?
李四-->王五: 很好!


Alice->>Bob: Hello Bob, how are you?
    alt is sick
    Bob->>Alice: Not so good :(
    else is well
    Bob->>Alice: Feeling fresh like a daisy
    end
    
    opt Extra response
    Bob->>Alice: Thanks for asking
    end
```







#### 3.2.甘特图表

```mermaid
%% Example with selection of syntaxes

gantt
dateFormat  YYYY-MM-DD
title 软件开发甘特图

section 设计
需求: done, des1, 2014-01-06, 2014-01-08
原型: active, des2, 2014-01-09, 3d
UI设计: des3, after des2, 5d
未来任务: des4, after des3, 5d

section 开发
理解需求: crit, done, 2014-01-06, 24h
设计框架: crit, done, after des2, 2d
开发: crit, active, 3d
未来任务: crit, 1w
娱乐: 2d

section 测试
功能测试: active, a1, after des3, 3d
压力测试: after a1, 20h
测试报告: 48h
```







#### 3.3.流程图

##### 3.3.1.横向流程图

```mermaid
%% 标签为 flowchart 也可以，线段的渲染方式会更多
graph LR
    A[方形]-->B(圆角)
    B --> C{条件a}
    C -->|a=1| D[结果1]
    C -->|a=2| E[结果2]
    C --> F[其他]
    G[横向流程图]
```







##### 3.3.2.纵向流程图

```mermaid
%% 标签为 flowchart 也可以，线段的渲染方式会更多
graph TD
	A[方形] -->B(圆角)
  	B --> C{条件a}
  	C -->|a=1| D[结果1]
  	C -->|a=2| E[结果2]
 	C --> F[其他]
    G[纵向流程图]
```









#### 3.4.状态图

```mermaid
stateDiagram-v2  # 新版渲染器
    [*] --> Still
    Still --> [*]

    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```







#### 3.5.饼图

```mermaid
pie
    title Pie Chart
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 150 
```





#### 3.6.后端类图

+ 类与类之间的关系：

  | 类型           | 描述                                                         | 示例                      | 示例描述           |
  | -------------- | ------------------------------------------------------------ | ------------------------- | ------------------ |
  | --\|> 或 <\|-- | 继承(Inheritance)：类的继承                                  | Animal<\|--Bird           | 子类指向父类       |
  | ..\|> 或 <\|.. | 实现(Realization)：接口实现                                  |                           | 子类指向父接口     |
  | *-- 或 --\*    | 组成(Composition)：组成是一种强的拥有关系，即contains-a的关系，体现了严格的部分和整体的关系，部分和整体的生命周期一样。 | 鸟 *-- 翅膀               | 成分指向整体       |
  | o-- 或 --o     | 聚合(Aggregation)：表示一种弱的拥有关系，即has-a的关系，体现的是A对象可以包含B对象，但B类不是A类的一部分。两个对象有各自的生命周期。 | 雁群 o-- 大雁             | 单体指向整体       |
  | --> 或 <--     | 关联(Association)：对于两个独立的对象，当一个对象的实例与另一个对象的特定实例存在固定的对应关系时，这两个对象之间构成关联关系。 | 书籍 <-- 作者(成员关联)   | 拥有者指向被拥有者 |
  | ..> 或 <..     | 依赖(Dependency)：对于两个独立的对象，当需要在一个对象中构造另一个对象的实例，用作方法的调用者或方法的形参时，两者构成关联关系。 | 计算机 <.. 人类(方法依赖) | 依赖者指向被依赖者 |
  | --             | Link(Solid)                                                  |                           |                    |
  | ..             | Link(Dashed)                                                 |                           |                    |

+ 双向关系：

  可以从下列关系选择一个：

  | 类型 | 描述 |
  | ---- | ---- |
  | <\|  | 继承 |
  | *    | 组成 |
  | o    | 聚合 |
  | >    | 关联 |
  | <    | 关联 |
  | \|>  | 实现 |

  再从下列连接方式中选择一个

  | 类型 | 描述       |
  | ---- | ---------- |
  | --   | 坚固，实线 |
  | ..   | 脆弱，虚线 |

+ 关系的多重性

  类图中的多重性或基本性表示一个类与另一类的一个实例关联的实例数。例如，一家公司将有一名或多名员工，但每位员工只为一家公司工作。多个注释放置在关联的末端附近。不同的基本选择是：

  | 类型 | 描述      |
  | ---- | --------- |
  | 1    | 只有1     |
  | 0..1 | 0或1      |
  | 1..* | 1或多     |
  | *    | 多        |
  | n    | n(n>1)    |
  | 0..n | 0到n(n>1) |
  | 1..n | 1到n(n>1) |



+ 根据上述语法，下图是一个示例程序：

  ```mermaid
  %%下面是mermaid代码:
  
  classDiagram
  direction RL
  class 动物{
  	+声明
  	+新陈代谢()
  	+繁殖()
  }
  class 鸟{
  	+羽毛
  	+下蛋()
  }
  class 鸭子{
  	+下蛋()
  }
  class 大雁{
  	+下蛋()
  	+飞()
  }
  class 企鹅{
  	+下蛋()
  }
  class 雁群
  class 唐老鸭
  class 气候
  class 翅膀
  class 氧气
  class 水
  class 飞翔
  class 讲话
  
  
  动物 ..|> 氧气: 依赖
  动物 ..> 水: 依赖
  鸟 --|> 动物
  大雁 --|> 鸟
  鸭子 --|> 鸟
  企鹅 --|> 鸟
  唐老鸭 --|> 鸭子
  雁群 "1" --o "n" 大雁: 聚合
  企鹅 --> 气候: 关联
  鸟 "1"*-- "2" 翅膀: 组合
  唐老鸭 ..|> 讲话
  大雁 ..|> 飞翔
  
  ```

  

+ 类、方法、可见性修饰

  + 类的修饰：<<interface>>接口, <<abstract>>抽象类, <<service>>服务类, <<enumeration>>枚举类
  + 方法的修饰：* abstract,  $ static，只可以在方法的末尾添加
  + 可见性：+ 表示 public, # 表示 protect, 无 表示 default，- 表示 private，~包/内部
  + 泛型：~SORT~，不支持嵌套类型的泛型
  + 变量修饰：$ static，只可以在变量的末尾添加

```mermaid
classDiagram

%% 声明类的第一种方式
Animal <|-- Duck
Animal <|-- Fish
Animal <|-- Zebra
Animal: +int age
Animal: +bool flag$
Animal: #String gender
Animal: List~String~ food
Animal: isMammal(int, List~String~)* int
Animal: -mate()$ double
Animal: -getFood() List~String~
Animal: <<interface>>

%% 声明类的第二种方式
class Duck{
    +String beakColor
    +swim()
    +quack()
    fly()
}
class Fish{
    -int sizeInFeet
    -canEat()
}
<<abstract>> Fish
```



> 注释：
>
> + 更多的 mermaid 语法可以参考 [mermaid官方文档](https://mermaid-js.github.io/mermaid/#/)
>
>   

